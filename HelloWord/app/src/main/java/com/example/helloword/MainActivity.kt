package com.example.helloword

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    private lateinit var textViewResultado: TextView
    private lateinit var textViewHistorico: TextView
    private var resultMode: Boolean = true
    private var n1 = 0.0
    private var op = ""
    private var n2 = 0.0
    private var lastDigit = ""
    var aux = ""
    var auxR = 0.0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        this.textViewResultado = findViewById(R.id.textViewResultado)
        this.textViewHistorico = findViewById(R.id.textViewHistorico)
        this.initValues()
    }

    private fun add(a: Double, b: Double): Double = a + b
    private fun sub(a: Double, b: Double): Double = a - b
    private fun mul(a: Double, b: Double): Double = a * b
    private fun div(a: Double, b: Double): Double = a / b
    private fun mod(a: Double, b: Double): Double = a % b

    fun initValues(){
        this.n1 = 0.0
        this.resultMode = true
        this.n2 = 0.0
        this.textViewHistorico.text = ""
        this.textViewResultado.text = "0.0"
    }

    fun clickLimpar(v: View){
        this.initValues()
    }

    fun clickNumero(v: View){

        val digit = (v as Button).text.toString()
        if(this.resultMode){
            this.textViewResultado.text = ""
        }
            this.textViewResultado.append(digit)
        n2 = textViewResultado.text.toString().toDouble()

        this.resultMode = false
    }


    fun clickOperation(v: View){
        val digit = (v as Button).text.toString()

        if(this.resultMode){

            aux = textViewHistorico.text.toString()
            textViewHistorico.text =  this.aux.substring(0, aux.length -1) + digit

        }else{
            Log.i("n1", n1.toString())
            Log.i("n2", n2.toString())
            Log.i("digit", lastDigit)

            textViewHistorico.text = textViewHistorico.text.toString() + n2 + digit

            if(lastDigit.isNotEmpty()) {
                auxR = operation(lastDigit.toString(), n2, n1)
                n1 = auxR
            }else{
                n1 = n2
            }
            lastDigit = digit


            textViewResultado.text = n1.toString()

        }
        this.resultMode = true

    }

    fun clickApagar (v: View){
        if(this.textViewResultado.length() > 0){

            Log.i("n1", n1.toString())
            Log.i("n2", n2.toString())
            Log.i("lastDigit", lastDigit)
            var aux = ""
            aux = this.textViewResultado.text.toString()
            aux = aux.substring(0,aux.length -1)
            this.textViewResultado.text = aux
        }
    }

    fun clickIgual(v: View){

        if(lastDigit != "") {
            n1 = operation(lastDigit, n2, n1)

            this.textViewHistorico.text =
                this.textViewHistorico.text.toString() + n2.toString() + lastDigit
            this.textViewResultado.text = n1.toString()

            resultMode = true
        }
    }

    fun operation(op: String, a: Double, b: Double): Double {
        return when(op) {
            "+" ->  add(a,b)
            "-" ->  sub(a,b)
            "*" ->  mul(a,b)
            "/" ->  div(a,b)
            "%" ->  mod(a,b)
            else -> {
                throw IllegalArgumentException(
                    "Operação não reconhecida..."
                )
            }
        }
    }
}
